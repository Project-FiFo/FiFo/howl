echo "$(git symbolic-ref HEAD 2> /dev/null | cut -b 12-)-$(git log --pretty=format:"%h, %ad" -1)" > howl.version
echo "-define(VERSION, <<\"$(cat howl.version)\">>)." > apps/howl/include/howl_version.hrl
